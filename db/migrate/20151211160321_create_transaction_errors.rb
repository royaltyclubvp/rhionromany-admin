class CreateTransactionErrors < ActiveRecord::Migration
  def change
    create_table :transaction_errors do |t|
      t.integer :order_id
      t.text :message

      t.timestamps null: false
    end
    add_index :transaction_errors, :order_id
  end
end
