class ChangeStatusDefaultInEnquiries < ActiveRecord::Migration
  def change
    change_column_default :enquiries, :status, 0
  end
end
