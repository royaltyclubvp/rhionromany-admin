class AddItemForSaleToCollectionItems < ActiveRecord::Migration
  def change
    add_column :collection_items, :item_for_sale, :boolean, default: false
  end
end
