class CreateCollectionItems < ActiveRecord::Migration
  def change
    create_table :collection_items do |t|
      t.string :name
      t.integer :collection_id
      t.integer :image_id

      t.timestamps null: false
    end
    add_index :collection_items, :collection_id
  end
end
