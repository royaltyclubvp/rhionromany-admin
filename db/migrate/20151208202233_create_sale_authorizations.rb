class CreateSaleAuthorizations < ActiveRecord::Migration
  def change
    create_table :sale_authorizations do |t|
      t.integer :collection_item_id
      t.integer :enquiry_id
      t.integer :price_cents
      t.integer :status, default: 0
      t.datetime :valid_to
      t.string :authorization_code, unique: true
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :sale_authorizations, :collection_item_id
    add_index :sale_authorizations, :enquiry_id
    add_index :sale_authorizations, :authorization_code
    add_index :sale_authorizations, :user_id
  end
end
