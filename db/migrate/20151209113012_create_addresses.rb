class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :street_address_1
      t.string :street_address_2
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :country
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :addresses, :user_id
  end
end
