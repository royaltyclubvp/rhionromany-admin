class RenamePaymentIdInOrders < ActiveRecord::Migration
  def change
    rename_column :orders, :payment_id, :transaction_id
  end
end
