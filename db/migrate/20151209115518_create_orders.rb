class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :sale_authorization_id
      t.integer :collection_item_id
      t.integer :address_id
      t.integer :user_id
      t.integer :quantity
      t.string :bra_size
      t.string :bust_size
      t.string :waist_size
      t.string :hip_size
      t.text :additional_info
      t.string :payment_id
      t.integer :payment_cents
      t.integer :status, default: 0

      t.timestamps null: false
    end
    add_index :orders, :sale_authorization_id
    add_index :orders, :collection_item_id
    add_index :orders, :address_id
    add_index :orders, :user_id
  end
end
