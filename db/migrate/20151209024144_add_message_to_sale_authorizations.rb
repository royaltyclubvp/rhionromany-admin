class AddMessageToSaleAuthorizations < ActiveRecord::Migration
  def change
    add_column :sale_authorizations, :message, :text
  end
end
