class AddOrderedToCollection < ActiveRecord::Migration
  def change
    add_column :collections, :ordered, :boolean, :default => false
  end
end
