class CreateEnquiries < ActiveRecord::Migration
  def change
    create_table :enquiries do |t|
      t.integer :collection_item_id
      t.integer :user_id
      t.text :message
      t.boolean :open, default: true

      t.timestamps null: false
    end
    add_index :enquiries, :collection_item_id
    add_index :enquiries, :user_id
  end
end
