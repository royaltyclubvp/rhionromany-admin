class ChangeImageIdInCollectionItems < ActiveRecord::Migration
  def change
    change_column :collection_items, :image_id, :string
  end
end
