class AddSlugToCollectionItems < ActiveRecord::Migration
  def change
    add_column :collection_items, :slug, :string
    add_index :collection_items, :slug, unique: true
  end
end
