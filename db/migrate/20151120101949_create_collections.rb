class CreateCollections < ActiveRecord::Migration
  def change
    create_table :collections do |t|
      t.string :name
      t.integer :year
      t.string :slug
      t.boolean :available

      t.timestamps null: false
    end
    add_index :collections, :slug, unique: true
  end
end
