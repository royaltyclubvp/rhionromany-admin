class RemoveOpenFromEnquiries < ActiveRecord::Migration
  def change
    remove_column :enquiries, :open, :boolean
  end
end
