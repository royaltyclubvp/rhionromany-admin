class CreateResponses < ActiveRecord::Migration
  def change
    create_table :responses do |t|
      t.integer :enquiry_id
      t.text :message
      t.references :writer, polymorphic: true

      t.timestamps null: false
    end
    add_index :responses, :enquiry_id
  end
end
