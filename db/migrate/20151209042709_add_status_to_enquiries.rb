class AddStatusToEnquiries < ActiveRecord::Migration
  def change
    add_column :enquiries, :status, :integer

    Enquiry.all.each do |enquiry|
      if enquiry.open?
        enquiry.opened!
      else
        enquiry.closed!
      end
    end
  end
end

