class AuthorizationMailerPreview < ActionMailer::Preview
  def authorization
    AuthorizationMailer.authorization(Response.first)
  end
end