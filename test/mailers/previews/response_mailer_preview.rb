class ResponseMailerPreview < ActionMailer::Preview
  def response_sent
    ResponseMailer.response_sent(Response.first)
  end
end