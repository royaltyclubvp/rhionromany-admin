module TabsHelper
  def tabs_for(tabs)
    TabSet.new(self, tabs).html
  end

  class TabSet
    def initialize(view, tabs_content)
      @view, @tabs_content = view, tabs_content
    end

    def html
      view.safe_join([tab_list, tab_content])
    end

    private

    attr_accessor :view, :tabs_content
    delegate :safe_join, :link_to, :content_tag, to: :view

    def tab_list
      items = tabs_content.count.times.map { |index| tab(index) }
      content_tag(:ul, safe_join(items), {class: 'nav nav-tabs', role: 'tablist'})
    end

    def tab(index)
      tab_class = 'nav-link'
      if index.zero?
        tab_class += ' active'
      end
      content_tag(:li, link_to(tabs_content.keys[index].to_s.capitalize, "##{tabs_content.keys[index].to_s}", class: tab_class, role: 'tab', data: {toggle: 'tab'}), {class: 'nav-item'})
    end

    def tab_content
      items = tabs_content.count.times.map { |index| tab_pane(index) }
      content_tag(:div, safe_join(items), {class: 'tab-content'})
    end

    def tab_pane(index)
      pane_class = 'tab-pane'
      if index.zero?
        pane_class += ' active'
      end
      content_tag(:div, tabs_content[tabs_content.keys[index]], {class: pane_class, id: tabs_content.keys[index].to_s, role: 'tabpanel'})
    end

  end
end