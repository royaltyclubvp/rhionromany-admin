class ResponseMailer < ApplicationMailer
  default from: 'enquiries@rhionromany.com'

  def response_sent(response)
    @response = response
    mail(to: @response.enquiry.user.email, reply_to: 'info@rhionromany.com', subject: 'Response Received')
  end
end
