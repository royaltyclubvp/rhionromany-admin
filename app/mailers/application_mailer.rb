class ApplicationMailer < ActionMailer::Base
  default from: "noreply@rhionromany.com"
  layout 'mailer'
end
