class AuthorizationMailer < ApplicationMailer
  default from: 'enquiries@rhionromany.com'

  def authorization(auth)
    @auth = auth
    mail(to: @auth.user.email, reply_to: 'info@rhionromany.com', subject: 'Purchase Authorization Granted')
  end

end
