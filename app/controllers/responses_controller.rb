class ResponsesController < ApplicationController
  before_action :authenticate_admin!

  # List Message Thread
  def index
    @enquiry = Enquiry.includes(:user, :responses).find(params[:enquiry_id])
  end

  # Write new RESPONSE
  def new
    @enquiry = Enquiry.includes(:user, :responses).find(params[:enquiry_id])
  end

  # Save RESPONSE
  def create
    response = Response.new(response_params)
    response.writer = current_admin
    if response.save
      redirect_to enquiry_responses_path(params[:enquiry_id]), success: 'Response sent'
    else
      redirect_to new_enquiry_response_path(params[:enquiry_id]), error: 'Sending failed'
    end
  end

  private

    def response_params
      params.require(:response).permit(:message, :enquiry_id)
    end
end
