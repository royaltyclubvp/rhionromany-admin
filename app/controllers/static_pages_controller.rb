class StaticPagesController < ApplicationController
  before_action :authenticate_admin!

  # Home Page
  def home
    @hide_nav = true
  end
end
