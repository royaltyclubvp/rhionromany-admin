class SaleAuthorizationsController < ApplicationController
  before_action :authenticate_admin!

  def create
    SaleAuthorization.create(auth_params)
    Enquiry.find(params[:enquiry_id]).authorized!
    redirect_to enquiries_path, notice: 'Authorization issued!'
  end

  def new
    @enquiry = Enquiry.includes(:user, collection_item: :collection).find(params[:enquiry_id])
  end

  def edit
    @enquiry = Enquiry.includes(:user, collection_item: :collection).find(params[:enquiry_id])
  end

  def update
    Enquiry.find(params[:enquiry_id]).sale_authorization.update(auth_params)
    redirect_to enquiries_path, notice: 'Changes to authorization saved!'
  end

  def destroy
    Enquiry.find(params[:enquiry_id]).closed!
    redirect_to enquiries_path, notice: 'Authorization cancelled!'
  end

  private
    def auth_params
      params.require(:sale_authorization).permit(:enquiry_id, :collection_item_id, :valid_to, :price, :user_id, :message)
    end
end
