class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper ApplicationHelper, TabsHelper

  add_flash_types :error, :success

  def authenticate_admin!
    session['admin_return_to'] = request.fullpath
    super
  end

  # Set post Sign In path
  def after_sign_in_path_for(resource)
    session.delete('admin_return_to') || root_path
  end
end
