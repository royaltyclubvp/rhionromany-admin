class EnquiriesController < ApplicationController
  before_action :authenticate_admin!

  # Show all ENQUIRIES
  def index
    @enquiries = Enquiry.includes(:user, collection_item: :collection).all
  end

  # Show Enquiry Details
  def show
    @enquiry = Enquiry.includes(:user, collection_item: :collection).find(params[:id])
  end

  # Close Enquiry
  def close
    if Enquiry.find(params[:id]).closed!
      flash[:success] = 'Enquiry closed'
    else
      flash[:error] = 'Closing of enquiry failed'
    end
    redirect_to enquiries_path
  end

  # Open  Enquiry
  def open
    if Enquiry.find(params[:id]).opened!
      flash[:success] = 'Enquiry opened'
    else
      flash[:error] = 'Opening of enquiry failed'
    end
    redirect_to enquiries_path
  end
end
