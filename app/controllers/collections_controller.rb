class CollectionsController < ApplicationController
  before_action :authenticate_admin!

  # List of Collections
  def index
    @collections = Collection.all
  end

  def show
    @collection = Collection.includes(:collection_items).friendly.find(params[:id])
  end

  # New Collection Form
  def new
    @collection = Collection.new
  end

  # Create Collection
  def create
    collection = Collection.new(collection_params)
    if collection.save
      redirect_to collection_path(collection), success: 'Collection added successfully'
    else
      redirect_to new_collection_path, error: 'Creation of collection failed. Please try again'
    end
  end

  # Edit existing Collection
  def edit
    @collection = Collection.friendly.find(params[:id])
  end

  # Update Collection
  def update
    if Collection.friendly.find(params[:id]).update_attributes(collection_params)
      redirect_to collection_path(params[:id]), success: 'Collection updated'
    else
      redirect_to edit_collection_path(params[:id]), error: 'Updated failed'
    end
  end

  def destroy
    if Collection.friendly.find(params[:id]).destroy
      flash[:success] = 'Collection deleted'
    else
      flash[:error] = 'Deletion failed. Please try again'
    end
    redirect_to collections_path
  end

  private
    def collection_params
      params.require(:collection).permit(:name, :year, :available, :ordered, collection_items_attributes: [:name, :image])
    end
end
