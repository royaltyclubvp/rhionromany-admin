class CollectionItemsController < ApplicationController
  before_action :authenticate_admin!

  # Add New Collection Items
  def new
    @collection = Collection.friendly.find(params[:collection_id])
  end

  # Create New Item
  def create
    if CollectionItem.create(item_params)
      redirect_to collection_path(params[:collection_id]), success: 'Item added'
    else
      redirect_to collection_path(params[:collection_id]), error: 'Attempt failed'
    end
  end

  # Delete Item
  def destroy
    if CollectionItem.friendly.find(params[:id]).destroy
      flash[:success] = 'Item deleted'
    else
      flash[:error] = 'Deletion failed'
    end
    redirect_to collection_path(params[:collection_id])
  end

  # Set Item as Image
  def image
    if CollectionItem.friendly.find(params[:id]).update_attributes({item_for_sale: false})
      flash[:success] = 'Item set as image'
    else
      flash[:error] = 'Changed failed. Please try again'
    end
    redirect_to collection_path(params[:collection_id])
  end

  def commercial_item
    if CollectionItem.friendly.find(params[:id]).update_attributes({item_for_sale: true})
      flash[:success] = 'Item set for sale'
    else
      flash[:error] = 'Changed failed. Please try again'
    end
    redirect_to collection_path(params[:collection_id])
  end

  private
    def item_params
      params.require(:collection_item).permit(:name, :image, :collection_id, :item_for_sale)
    end
end
