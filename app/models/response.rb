# == Schema Information
#
# Table name: responses
#
#  id          :integer          not null, primary key
#  enquiry_id  :integer
#  message     :text
#  writer_id   :integer
#  writer_type :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_responses_on_enquiry_id  (enquiry_id)
#

class Response < ActiveRecord::Base
  # Relationships
  #
  # Belongs to a WRITER
  belongs_to :writer, polymorphic: true

  # Belongs to an ENQUIRY
  belongs_to :enquiry, inverse_of: :responses

  # Model Callbacks
  #
  # After Save, -> Send Email Notification
  after_save :send_email_notification

  # Convenience Methods
  #
  # Enquiry Response URL
  def user_response_url
    ENV['site_host'] + '/account/enquiries/' + self.enquiry.id.to_s + '/responses'
  end

  # Service Methods
  def send_email_notification
    ResponseMailer.response_sent(self).deliver_now
  end

end
