# == Schema Information
#
# Table name: addresses
#
#  id               :integer          not null, primary key
#  street_address_1 :string
#  street_address_2 :string
#  city             :string
#  state            :string
#  zip_code         :string
#  country          :string
#  user_id          :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_addresses_on_user_id  (user_id)
#

class Address < ActiveRecord::Base
end
