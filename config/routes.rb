# == Route Map
#
#                                     Prefix Verb   URI Pattern                                                                Controller#Action
#                                       root GET    /                                                                          static_pages#home
#                          new_admin_session GET    /login(.:format)                                                           devise/sessions#new
#                              admin_session POST   /login(.:format)                                                           devise/sessions#create
#                      destroy_admin_session DELETE /logout(.:format)                                                          devise/sessions#destroy
# commercial_item_collection_collection_item POST   /collections/:collection_id/collection_items/:id/commercial-item(.:format) collection_items#commercial_item
#           image_collection_collection_item POST   /collections/:collection_id/collection_items/:id/image(.:format)           collection_items#image
#                collection_collection_items GET    /collections/:collection_id/collection_items(.:format)                     collection_items#index
#                                            POST   /collections/:collection_id/collection_items(.:format)                     collection_items#create
#             new_collection_collection_item GET    /collections/:collection_id/collection_items/new(.:format)                 collection_items#new
#            edit_collection_collection_item GET    /collections/:collection_id/collection_items/:id/edit(.:format)            collection_items#edit
#                 collection_collection_item GET    /collections/:collection_id/collection_items/:id(.:format)                 collection_items#show
#                                            PATCH  /collections/:collection_id/collection_items/:id(.:format)                 collection_items#update
#                                            PUT    /collections/:collection_id/collection_items/:id(.:format)                 collection_items#update
#                                            DELETE /collections/:collection_id/collection_items/:id(.:format)                 collection_items#destroy
#                                collections GET    /collections(.:format)                                                     collections#index
#                                            POST   /collections(.:format)                                                     collections#create
#                             new_collection GET    /collections/new(.:format)                                                 collections#new
#                            edit_collection GET    /collections/:id/edit(.:format)                                            collections#edit
#                                 collection GET    /collections/:id(.:format)                                                 collections#show
#                                            PATCH  /collections/:id(.:format)                                                 collections#update
#                                            PUT    /collections/:id(.:format)                                                 collections#update
#                                            DELETE /collections/:id(.:format)                                                 collections#destroy
#                              close_enquiry DELETE /enquiries/:id/close(.:format)                                             enquiries#close
#                               open_enquiry POST   /enquiries/:id/open(.:format)                                              enquiries#open
#                          enquiry_responses GET    /enquiries/:enquiry_id/responses(.:format)                                 responses#index
#                                            POST   /enquiries/:enquiry_id/responses(.:format)                                 responses#create
#                       new_enquiry_response GET    /enquiries/:enquiry_id/responses/new(.:format)                             responses#new
#                 enquiry_sale_authorization POST   /enquiries/:enquiry_id/sale_authorization(.:format)                        sale_authorizations#create
#             new_enquiry_sale_authorization GET    /enquiries/:enquiry_id/sale_authorization/new(.:format)                    sale_authorizations#new
#            edit_enquiry_sale_authorization GET    /enquiries/:enquiry_id/sale_authorization/edit(.:format)                   sale_authorizations#edit
#                                            GET    /enquiries/:enquiry_id/sale_authorization(.:format)                        sale_authorizations#show
#                                            PATCH  /enquiries/:enquiry_id/sale_authorization(.:format)                        sale_authorizations#update
#                                            PUT    /enquiries/:enquiry_id/sale_authorization(.:format)                        sale_authorizations#update
#                                            DELETE /enquiries/:enquiry_id/sale_authorization(.:format)                        sale_authorizations#destroy
#                                  enquiries GET    /enquiries(.:format)                                                       enquiries#index
#                                    enquiry GET    /enquiries/:id(.:format)                                                   enquiries#show
#                                 refile_app        /attachments                                                               #<Refile::App app_file="/Users/royaltyclubvp/.rvm/gems/ruby-2.2.3/gems/refile-0.6.2/lib/refile/app.rb">
#

Rails.application.routes.draw do

  root 'static_pages#home'

  devise_for :admins,
             path: '',
             path_names: {
                 sign_in: 'login',
                 sign_out: 'logout'
             }

  resources :collections do
    resources :collection_items do
      member do
        post 'commercial-item', to: 'collection_items#commercial_item'
        post 'image'
      end
    end
  end

  resources :enquiries, only: [:index, :show] do
    member do
      delete 'close'
      post 'open'
    end
    resources :responses, only: [:new, :create, :index]
    resource :sale_authorization, except: [:index]
  end
end
