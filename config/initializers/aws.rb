if Rails.env.development?
  Aws.config[:ssl_ca_bundle] = '/usr/local/etc/openssl/cert.pem'
end
